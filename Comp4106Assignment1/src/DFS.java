import java.util.ArrayList;
import java.util.LinkedList;

public class DFS extends KnightAI {
	private LinkedList<Node> visitedList;
	private LinkedList<Node> toVisitList;
	public DFS(Board b, Knight k){
		super(b,k);
	}
	
	
	public LinkedList<Node> calcMoves(Vector2 start, Vector2 end) {
		// TODO Auto-generated method stub
		visitedList = new LinkedList<Node>();//closed
		toVisitList = new LinkedList<Node>();//open
		Node StartNode = new Node(start);
		StartNode.setParent(null);
		toVisitList.add(StartNode);
		while(!toVisitList.isEmpty()){
		//	System.out.println("iterate");
			Node node = toVisitList.removeLast();
			if(node.getPos().isEqual(end.getX(), end.getY())){
				return constructPath(node);
			}
			else{
				visitedList.add(node);
				ArrayList<Vector2> pos = this.filterPaths(k.getMoves(node.getPos()));
				for(Vector2 v2 : pos){
					if(!checkPosVisited(v2)&&!checkPosToVisited(v2)){
						Node child = new Node(v2);
						child.setParent(node);
						toVisitList.add(child);
					}
				}
			}
		}
		
		return null;
	}
	
	public boolean checkPosVisited(Vector2 v2){
		for(Node n : this.visitedList){
			if(n.getPos().isEqual(v2.getX(), v2.getY())){
				return true;
			}
		}
		return false;
	}
	
	public boolean checkPosToVisited(Vector2 v2){
		for(Node n : this.toVisitList){
			if(n.getPos().isEqual(v2.getX(), v2.getY())){
				return true;
			}
		}
		return false;
	}
	
	
	
	public LinkedList<Node> constructPath(Node node){
		LinkedList<Node> path = new LinkedList();
		while(node.getParent()!= null){
			path.addFirst(node);
			node = node.getParent();
		}
		
		return path;
	}

}
