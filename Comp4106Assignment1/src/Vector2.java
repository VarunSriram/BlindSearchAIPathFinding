
public class Vector2 {
	private int x;
	private int y;
	
	public Vector2(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public int getX(){
		return this.x;
	}
	
	public int getY(){
		return this.y;
	}
	
	public boolean isEqual(int x, int y){
		return this.x ==x && this.y == y;
	}
	
	public boolean isEqual(Vector2 v2){
		return this.x ==v2.getX() && this.y==v2.getY();
	}
	
	public boolean isAdjacent (Vector2 v2){
		return v2.getY() == this.y+1 || v2.getY() == this.y-1 || 
			   v2.getX() == this.x-1 || v2.getX() == this.x+1 || 
			   v2.getY() == this.y+2 || v2.getY() == this.y-2 || 
			   v2.getX() == this.x-2 || v2.getX() == this.x+2;
	}
	
	
	public Vector2 addVector2(Vector2 v){
		return new Vector2(this.x+v.getX(),this.y+v.getY());
		
	}
	
	public float distance(Vector2 v){
		 float dist = Math.abs(((v.x - this.x))+ Math.abs((v.y - this.y)));
		 return dist;
	}
	public float averageHeuristic(Vector2 v){
		float dist = Math.abs(((v.x - this.x))+ Math.abs((v.y - this.y)));
		float approxKnightMovesX = Math.abs(v.x - this.x)/2;
		float approxKnightMovesY = Math.abs(v.y - this.y)/2;
		return (dist+approxKnightMovesX+approxKnightMovesY)/3;
		
		
	}
	
	
}
