
import java.util.List;


public class AStarNode extends Node implements Comparable<Object> {
	 
	 float f;
	 float g,h;
	 
  public AStarNode(Vector2 p) {
		super(p);
		// TODO Auto-generated constructor stub
	}




  public float getCost() {
    return g + h;
  }


  public int compareTo(Object other) {
    float thisValue = this.getCost();
    float otherValue = ((AStarNode)other).getCost();

    float v = thisValue - otherValue;
    return (v>0)?1:(v<0)?-1:0; // sign function
  }

  public float getCost(AStarNode node){
	 return this.pos.distance(node.getPos());
  }


  
  public float getEstimatedCost(AStarNode node){
	 return this.pos.distance(node.getPos());
			 
  }
  
}  