import java.util.ArrayList;

public class Knight extends ChessPiece {
	private final Vector2 m1 = new Vector2(1,2),
						  m2 = new Vector2(-1,2),
						  m3 = new Vector2(1,-2),
						  m4 = new Vector2(-1,-2),
						  m5 = new Vector2(2,1),
						  m6 = new Vector2(2,-1),
						  m7 = new Vector2(-2,1),
						  m8 = new Vector2(-2,-1);
							 
	
	
	public Knight(Vector2 start){
		super(start);
	
	}
	
	public ArrayList <Vector2> getMoves(){
		ArrayList<Vector2> movePackage = new ArrayList<Vector2>();
		movePackage.add(this.currPos.addVector2(m1));
		movePackage.add(this.currPos.addVector2(m2));
		movePackage.add(this.currPos.addVector2(m3));
		movePackage.add(this.currPos.addVector2(m4));
		movePackage.add(this.currPos.addVector2(m5));
		movePackage.add(this.currPos.addVector2(m6));
		movePackage.add(this.currPos.addVector2(m7));
		movePackage.add(this.currPos.addVector2(m8));
		
		return movePackage;
		
	}
	
	public ArrayList<Vector2>getMoves(Vector2 pos){
		ArrayList<Vector2> movePackage = new ArrayList<Vector2>();
		movePackage.add(pos.addVector2(m1));
		movePackage.add(pos.addVector2(m2));
		movePackage.add(pos.addVector2(m3));
		movePackage.add(pos.addVector2(m4));
		movePackage.add(pos.addVector2(m5));
		movePackage.add(pos.addVector2(m6));
		movePackage.add(pos.addVector2(m7));
		movePackage.add(pos.addVector2(m8));
		return movePackage;
	}
	
	
}