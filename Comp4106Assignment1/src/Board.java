import java.util.ArrayList;

public class Board {
	private static final char KNIGHT = 'K';
	private static final char PAWN = 'p';
	private static final char EMPTY = '_';
	
	private char[][] chessBoard;
	private int size;
	private int minBound;
	private int maxBound;
	ArrayList<ChessPiece> pieces;
	
	public Board(int size, ArrayList<ChessPiece> pieces){
		this.chessBoard = new char[size][size];
		this.pieces = pieces;
		this.size = size;
		this.minBound = 0;
		this.maxBound = size-1;
		this.setEmptyBoard();
		
	}
	
	public void addPiece(ChessPiece cp){
		this.pieces.add(cp);
		this.refreshBoard();
	}
	
	public void removePiece(ChessPiece cp){
		this.pieces.remove(cp);
		this.refreshBoard();
	}
	
	public int getSize(){
		return size;
	}
	
	public boolean isOnEdge(Vector2 v){
		return v.getX() == minBound || v.getX() == maxBound || v.getY() == minBound || v.getY() == maxBound;
	}
	
	public boolean isOutside(Vector2 v){
		return v.getX()<minBound || v.getX() > maxBound || v.getY()<minBound || v.getY()>maxBound;
	}
	
	public boolean isValid(Vector2 v){
		if(isOnEdge(v) || isOutside(v)){
			return false;
		}
		else{
			return true;
		}
	}
	
	public void setEmpty(Vector2 v){
		this.chessBoard[v.getX()][v.getY()] = EMPTY;
	}
	
	public void setPawn(Vector2 v){
		this.chessBoard[v.getX()][v.getY()] = PAWN;
	}
	
	public void setKnight(Vector2 v){
		this.chessBoard[v.getX()][v.getY()] = KNIGHT;
	}
	
	public void refreshBoard(){
		this.setEmptyBoard();
		for(int i = 0; i<size;i++){
			for(int j = 0; j<size; j++){
				ChessPiece cp = this.pieceAt(i, j);
				if(cp instanceof Knight){
					this.setKnight(new Vector2(i,j));
				}
				if(cp instanceof Pawn){
					this.setPawn(new Vector2(i,j));
				}
			}
		}
		
		
	}
	
	
	public void printBoard(){
		
		this.refreshBoard();
		
	
		System.out.println("");
		for(int i = 0; i<size;i++){
			System.out.print(i);
			int digit = String.valueOf(size).length()-String.valueOf(i).length();
			for(int w = 0; w<=digit;w++){
				System.out.print(" ");
			}
			for(int j = 0; j<size; j++){
				
				System.out.print('|');
				
				System.out.print(this.chessBoard[i][j]);
				
				System.out.print('|');
			}
			System.out.println("");
		}
		System.out.println("");
		System.out.println("------------------------------END------------------------------");
	}
	
	public boolean isOccupied(Vector2 v){
		return this.chessBoard[v.getX()][v.getY()] != EMPTY;
	}
	
	private void setEmptyBoard(){
		for(int i = 0; i<size;i++){
			for(int j = 0; j<size; j++){
				this.chessBoard[i][j] = EMPTY;
			}
		}
	}
	
	private ChessPiece pieceAt(int x, int y){
		for(ChessPiece cp : pieces){
			if (cp.currPos.isEqual(x,y)){
				return cp;
			}
		}
		return null;
	}
	
	public void resolveColliions(){
		Knight k = findKnight();
		System.out.println("Pieces left: " + this.pieces.size());
		ArrayList<ChessPiece> match = new ArrayList<ChessPiece>();
		for(ChessPiece cp : pieces){
			if(!(cp instanceof Knight) && cp.currPos.isEqual(k.currPos)){
				System.out.println("HELLO");
				match.add(cp);
			}
		}
		
		for(ChessPiece m : match){
			this.pieces.remove(m);
		}
	
	}
	
	
	private Knight findKnight(){
		for(ChessPiece cp : pieces){
			if(cp instanceof Knight){
				return (Knight) cp;
			}
		}
		
		return null;
	}
	
	
	
}
