
public class ChessPiece {
	protected Vector2 startPos;
	protected Vector2 currPos;
	
	public ChessPiece (Vector2 start){
		startPos = start;
		currPos = start;
	}
	
	public void move (Vector2 dest){
		this.currPos = dest;
	}
}
