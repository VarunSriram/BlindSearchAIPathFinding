import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class AStarSearch extends KnightAI {
	private LinkedList<AStarNode> openList;
	private LinkedList<AStarNode> closedList;
	
	
	public AStarSearch(Board b, Knight k){
		super(b,k);
		 
	}
	
	@Override
	public LinkedList<Node> calcMoves(Vector2 start, Vector2 end) {
		openList = new LinkedList<AStarNode>();
		 closedList = new LinkedList<AStarNode>();
		AStarNode startNode = new AStarNode(start);
		AStarNode goalNode = new AStarNode(end);
		startNode.f = 0;
		openList.add(startNode);
		
		while(!openList.isEmpty()){
			AStarNode node = this.openList.remove(this.findIndexWithSmallestCost());
			ArrayList<Vector2> validMoves = this.filterPaths(k.getMoves(node.getPos()));
			ArrayList <AStarNode> sucessors = new ArrayList<AStarNode>();
			for(Vector2 v2 : validMoves){
				AStarNode child = new AStarNode(v2);
				child.setParent(node);
				sucessors.add(child);
				
			}
			
			for(AStarNode asn : sucessors){
				if (asn.getPos().isEqual(goalNode.getPos())){
					return this.constructPath(asn);
				}
				
				asn.g = node.g + node.getPos().averageHeuristic(asn.getPos());
				asn.h = asn.getPos().averageHeuristic(goalNode.getPos());
				asn.f = asn.g + asn.h;
				
				if(!checkOpenVisited(asn.getPos(),asn.f)&&!checkClosedVisited(asn.getPos(),asn.f)){
					this.openList.add(asn);
				}
				
			}
			
			this.closedList.add(node);
			
			
			
			
		}
		
		return null;
		
	}
	
	public boolean checkOpenVisited(Vector2 v2, float score){
		for(AStarNode n : this.openList){
			if( n.getPos().isEqual(v2.getX(), v2.getY())){
				if(n.f<score){
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean checkClosedVisited(Vector2 v2,float score){
		for(AStarNode n : this.closedList){
			if(n.getPos().isEqual(v2.getX(), v2.getY())){
				if(n.f<score){
					return true;
				}
			}
		}
		return false;
	}
	
	public int findIndexWithSmallestCost(){
		ArrayList<Float> a = new ArrayList<Float>();
		for(AStarNode n : openList){
			a.add(n.f);
		}
		int minIndex = a.indexOf(Collections.min(a));
		return minIndex;
	}

	 protected LinkedList<Node> constructPath(Node node) {
		    LinkedList<Node> path = new LinkedList<Node>();
		    while (node.getParent() != null) {
		      path.addFirst(node);
		      node = node.getParent();
		    }
		    return path;
		  }
	



}
