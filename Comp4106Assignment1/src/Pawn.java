import java.util.Random;

public class Pawn extends ChessPiece {
	private boolean hasMoved;
	
	public Pawn(Vector2 start){
		super(start);
		hasMoved = false;
	}
	
	
	public void move(){
		if(!hasMoved){
			int d = this.randInt(1, 4);
			switch(d){
			case 1:this.move(this.currPos.addVector2(new Vector2(0,1)));
			case 2: this.move(this.currPos.addVector2(new Vector2(1,0)));
			case 3: this.move(this.currPos.addVector2(new Vector2(-1,0)));
			case 4: this.move(this.currPos.addVector2(new Vector2(0,-1)));
			hasMoved = true;
			}
		}
		else{
			this.move(this.startPos);
			hasMoved = false;
		}
	}
	
	private int randInt(int min, int max) {

	    Random rand = new Random();

	    
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
	
}
