import java.util.ArrayList;
import java.util.LinkedList;

public abstract class KnightAI {
	protected Board b;
	protected Knight k;
	
	public KnightAI(Board b, Knight k){
		this.b = b;
		this.k = k;
	}
	
	public abstract LinkedList<Node> calcMoves(Vector2 start, Vector2 end);
	
	public ArrayList<Vector2> filterPaths(ArrayList<Vector2> n){
		ArrayList<Vector2> movePackage = new ArrayList<Vector2>();
		for(Vector2 v2 : n){
			if(b.isValid(v2)){
				movePackage.add(v2);
			}
		}
		return movePackage;
		
	}
}
