
public class Node {
	protected Node parent;
	protected Vector2 pos;
	public Node(Vector2 p){
		this.pos = p;
	}
	
	public void setParent(Node n){
		this.parent = n;
	}
	
	public Node getParent(){
		return this.parent;
	}
	
	public Vector2 getPos(){
		return this.pos;
	}
}
