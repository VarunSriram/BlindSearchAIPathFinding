import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

public class KnightGame {
	private Board b;
	private Knight knight;
	private Pawn[] pawns;
	private int numberOfPawns;
	private int size;
	private ArrayList<ChessPiece>pieces = new ArrayList<ChessPiece>();
	
	public KnightGame(int size, int pawns){
		this.size = size;
		this.numberOfPawns = pawns;
		this.pawns = new Pawn[pawns];
		b = new Board(size,pieces);
		this.setPawns();
		b.printBoard();
		this.setKnight();
		b.printBoard();
		
		
		
		KnightAI ai = new AStarSearch(b,this.knight);
		for(int i = 0; i < this.numberOfPawns; i++){
		System.out.println("TARGET:"+ this.pawns[i].startPos.getX()+" "+this.pawns[i].startPos.getY());
		LinkedList<Node> path = ai.calcMoves(this.knight.currPos,this.pawns[i].startPos);
		
		
		for(Node n : path){
			System.out.println("Knight to: "+n.getPos().getX()  +" "+n.getPos().getY());
			this.moveKnight(n.getPos());
			System.out.println("Moving Pawns");
			this.movePawns();
			
			
		}
		}
		
	}
	
	public void fixCollisions(){
		b.resolveColliions();
			
		}
	

	
	
	public void movePawns(){
		for(Pawn p : pawns){
			p.move();
			
		}
		this.fixCollisions();
		b.refreshBoard();
		b.printBoard();
	}
	
	public void moveKnight(Vector2 v2){
		this.knight.move(v2);
		this.fixCollisions();
		b.printBoard();
	}
	
	public void setPawns(){
		for(int i =0; i<numberOfPawns;i++){
			Vector2 initPos = this.getRandomPosition();
			while(this.pawnAdjacent(initPos)){
				initPos = this.getRandomPosition();
			}
			pawns[i] = new Pawn(initPos);
			System.out.println(i + " "+ pawns[i].currPos.getX()+" " + pawns[i].currPos.getY());
			b.addPiece(pawns[i]);
		}
		
	}
	
	private boolean pawnAdjacent(Vector2 v2){
		for(Pawn p : pawns){
			if(p != null){
				if(p.currPos.isAdjacent(v2)){
					return true;
				}
			}
		}
		return false;
	}
	
	public void setKnight(){
		knight = new Knight(this.getRandomPosition());
		b.addPiece(knight);
	}
	
	public Vector2 getRandomPosition(){
		int x = this.randInt(2, size - 3);
		int y = this.randInt(2, size -3);
		if(b.isOccupied(new Vector2(x,y))){
			return this.getRandomPosition();
		}
		else{
			return new Vector2(x,y);
		}
		
	}
	
	public int randInt(int min, int max) {

	    Random rand = new Random();

	    
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
}
